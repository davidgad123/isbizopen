//
//  RootViewController.h
//  IsBizOpen
//
//  Created by Rolex Jain on 10/24/13.
//
//

#import <UIKit/UIKit.h>
#import "Group.h"

@interface RootViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) NSArray *groups;
@property (strong, nonatomic) NSArray *favoriteGroups;

- (void) setFavoriteWithIndex:(NSInteger)index;
@end
