//
//  InBoxViewCell.h
//  IsBizOpen
//
//  Created by chamc on 12/13/13.
//
//

#import <UIKit/UIKit.h>

@interface InBoxViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *storeName;
@property (weak, nonatomic) IBOutlet UILabel *desc;
@property (weak, nonatomic) IBOutlet UILabel *validity;
@property (weak, nonatomic) IBOutlet UILabel *days;

@end
