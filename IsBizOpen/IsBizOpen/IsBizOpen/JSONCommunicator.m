
//

#import "JSONCommunicator.h"
#import "JSONCommunicatorDelegate.h"

@implementation JSONCommunicator

- (void)searchGroupsFromStore:(NSString*)storeName zipcode:(NSString*)zipcode latitude:(double)latitude longitude:(double)longitude time:(NSInteger)time day:(NSString*)day
{
    
    NSString* store = [storeName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString* code = [zipcode stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    NSString *urlAsString = [NSString stringWithFormat:@"http://isbizopen.com/api/getIsBizOpen.php?store=%@&zip=%@&userLoc=%f,%f&time=%d&day=%@",store, code, latitude, longitude, time, day];
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    NSLog(@"%@", urlAsString);
        
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (error) {
            [self.delegate fetchingGroupsFailedWithError:error];
        } else {
            [self.delegate receivedGroupsJSON:data];
        }
    }];
}

@end
