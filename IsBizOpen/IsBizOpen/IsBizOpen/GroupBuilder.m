//
//
//

#import "GroupBuilder.h"
#import "Group.h"

@implementation GroupBuilder
+ (NSArray *)groupsFromJSON:(NSData *)objectNotation error:(NSError **)error
{
/*    NSString* str= @"{\"result\":\"true\",\"entry\":[{\"poi_name\":\"Burger King\",\"store_id\":\"59380\",\"full_address\":\"175 W Calaveras Blvd Milpitas, CA 95035\",\"phone_number\":\"4082637131\",\"site_name\":\"\",\"is_open_24_hrs\":\"N\",\"open_time\":\"0600\",\"close_time\":\"2300\",\"openStatus\":\"Y\",\"todaysTiming\":\"06:00 am to 11:00 pm\"},{\"poi_name\":\"Burger King\",\"store_id\":\"59381\",\"full_address\":\"1475 Dempsey Rd Milpitas, CA 95035\",\"phone_number\":\"4082633313\",\"site_name\":\"\",\"is_open_24_hrs\":\"N\",\"open_time\":\"0600\",\"close_time\":\"2300\",\"openStatus\":\"Y\",\"todaysTiming\":\"06:00 am to 11:00 pm\"},{\"poi_name\":\"Burger King\",\"store_id\":\"59674\",\"full_address\":\"2532 Channing Ave San Jose, CA 95131\",\"phone_number\":\"4089440101\",\"site_name\":\"\",\"is_open_24_hrs\":\"N\",\"open_time\":\"0530\",\"close_time\":\"0000\",\"openStatus\":\"Y\",\"todaysTiming\":\"05:30 am to 12:00 am\"},{\"poi_name\":\"Burger King\",\"store_id\":\"59675\",\"full_address\":\"1181 Oakland Rd San Jose, CA 95112\",\"phone_number\":\"4082873332\",\"site_name\":\"\",\"is_open_24_hrs\":\"N\",\"open_time\":\"0600\",\"close_time\":\"2300\",\"openStatus\":\"Y\",\"todaysTiming\":\"06:00 am to 11:00 pm\"}],\"source\":\"google\"}";
    NSData* objectNotation=[str dataUsingEncoding:NSUTF8StringEncoding];*/
    
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:objectNotation options:0 error:&localError];
    
    if (localError != nil) {
        *error = localError;
        return nil;
    }
    
    NSMutableArray *groups = [[NSMutableArray alloc] init];
    
    NSArray *results = [parsedObject valueForKey:@"entry"];
    
    NSString* result = [parsedObject valueForKey:@"result"];
    
    if ([result isEqualToString:@"false"])
        return nil;
    
    NSLog(@"Count %d", results.count);
    for (NSDictionary *groupDic in results) {
        Group *group = [[Group alloc] init];
        
        for (NSString *key in groupDic) {
            NSString* lower_key = [key lowercaseString];
            if ([group respondsToSelector:NSSelectorFromString(lower_key)]) {
                [group setValue:[groupDic valueForKey:key] forKey:lower_key];
            }
        }
        
        [groups addObject:group];
    }
    
    return groups;
}
@end
