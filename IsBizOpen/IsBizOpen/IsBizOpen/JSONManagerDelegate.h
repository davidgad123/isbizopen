//
//  JSONManagerDelegate.h
//

#import <Foundation/Foundation.h>

@protocol JSONManagerDelegate
- (void)didReceiveGroups:(NSArray *)groups;
- (void)fetchingGroupsFailedWithError:(NSError *)error;
@end
