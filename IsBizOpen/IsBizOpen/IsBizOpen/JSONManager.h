//
//  JSONManager.h
//  BrowseMeetup
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "JSONManagerDelegate.h"
#import "JSONCommunicatorDelegate.h"

@class JSONCommunicator;

@interface JSONManager : NSObject<JSONCommunicatorDelegate>
@property (strong, nonatomic) JSONCommunicator *communicator;
@property (weak, nonatomic) id<JSONManagerDelegate> delegate;

- (void)fetchGroupsFromStore:(NSString*)storeName zipcode:(NSString*)zipcode latitude:(double)latitude longitude:(double)longitude time:(NSInteger)time day:(NSString*)day;
@end