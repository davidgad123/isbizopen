//
//  StoreViewCell.h
//  IsBizOpen
//
//  Created by Rolex Jain on 11/8/13.
//
//

#import <UIKit/UIKit.h>

@interface StoreViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *subTitle;
@end
