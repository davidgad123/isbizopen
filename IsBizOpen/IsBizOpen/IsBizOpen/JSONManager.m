//
//  JSONManager.m
//

#import "JSONManager.h"
#import "GroupBuilder.h"
#import "JSONCommunicator.h"

@implementation JSONManager
- (void)fetchGroupsFromStore:(NSString*)storeName zipcode:(NSString*)zipcode latitude:(double)latitude longitude:(double)longitude time:(NSInteger)time day:(NSString*)day
{
    [self.communicator searchGroupsFromStore:storeName zipcode:zipcode latitude:latitude longitude:longitude time:time day:day];
}

#pragma mark - JSONCommunicatorDelegate

- (void)receivedGroupsJSON:(NSData *)objectNotation
{
    NSError *error = nil;
    NSArray *groups = [GroupBuilder groupsFromJSON:objectNotation error:&error];
    
    if (error != nil) {
        [self.delegate fetchingGroupsFailedWithError:error];
        
    } else {
        [self.delegate didReceiveGroups:groups];
    }
}

- (void)fetchingGroupsFailedWithError:(NSError *)error
{
    [self.delegate fetchingGroupsFailedWithError:error];
}
@end
