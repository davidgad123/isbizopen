//
//  MainChildViewController.h
//  IsBizOpen
//
//  Created by Rolex Jain on 10/24/13.
//
//

#import <UIKit/UIKit.h>
#import "Group.h"
#import "RootViewController.h"

@interface MainChildViewController : UIViewController
{
    NSArray* weekDays;
}

@property (weak, nonatomic) IBOutlet UIView *moreInfoView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *nowLabel;
@property (weak, nonatomic) IBOutlet UILabel *moreInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *mapLabel;
@property (weak, nonatomic) IBOutlet UILabel *callTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *storeLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *nowImageView;
@property (weak, nonatomic) IBOutlet UILabel *remainTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *fromHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *toHoursLabel;
@property (weak, nonatomic) IBOutlet UIButton *mapImageView;
@property (weak, nonatomic) IBOutlet UILabel *mapDistanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *mapTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UILabel *callLabel;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;

@property (assign,nonatomic) NSInteger index;
@property (strong, nonatomic) Group* group;
@property (strong, nonatomic) RootViewController *rootViewController;

@property (nonatomic, retain) UITableView* moreInfoTableView;
@property (nonatomic, retain) NSMutableArray* moreInfos;

@end
