//
//  Communicator.h
//  IsBizOpen
//
//  Created by Rolex Jain on 11/10/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocationManager.h>
#import "Group.h"

@interface Communicator : NSObject

+ (NSArray*) getFavorites;
+ (NSArray*) getInboxes;
+ (NSArray*) setupDurationAndDistanceWithGroups:(NSArray*)groups latitude:(float)latitude longitude:(float)longitude;
+ (Group*) setupDurationAndDistanceWithGroup:(Group*)group latitude:(float)latitude longitude:(float)longitude;

+ (void) addFavoriteWithStoreId:(NSString*)storeId;
+ (void) removeFavoriteWithStoreId:(NSString*)storeId;
+ (Group*) getStoreWithId:(NSString*)storeId;
+ (void) refreshChannels;
@end
