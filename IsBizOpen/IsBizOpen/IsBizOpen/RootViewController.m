//
//  RootViewController.m
//  IsBizOpen
//
//  Created by Rolex Jain on 10/24/13.
//
//

#import "RootViewController.h"
#import "MainChildViewController.h"
#import "Communicator.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    
    MainChildViewController *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self setTitle:@""];
    
    _favoriteGroups = [Communicator getFavorites];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setFavoriteWithIndex:(NSInteger)index
{
    Group* group = _groups[index];
    group.isFavorite = true;
}

- (MainChildViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    MainChildViewController *childViewController = [[MainChildViewController alloc] initWithNibName:@"MainChildViewController" bundle:nil];
    childViewController.index = index;
    childViewController.group = self.groups[index];
    childViewController.rootViewController = self;
    
    for (Group *favoriteGroup in _favoriteGroups) {
        if ([childViewController.group.store_id isEqualToString:favoriteGroup.store_id]) {
            childViewController.group.isFavorite = true;
            break;
        }
    }
    
    return childViewController;
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(MainChildViewController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;

    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(MainChildViewController *)viewController index];
    
    index++;
    
    if (index == [self.groups count]) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return [self.groups count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

@end
