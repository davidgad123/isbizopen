
//

#import <Foundation/Foundation.h>

@interface Group : NSObject
@property (strong, nonatomic) NSString *poi_name;
@property (strong, nonatomic) NSString *store_id;
@property (strong, nonatomic) NSString *full_address;
@property (strong, nonatomic) NSString *phone_number;
@property (strong, nonatomic) NSString *site_name;
@property (strong, nonatomic) NSString *is_open_24_hrs;
@property (strong, nonatomic) NSString *open_time;
@property (strong, nonatomic) NSString *close_time;
@property (strong, nonatomic) NSString *openstatus;
@property (strong, nonatomic) NSString *todaystiming;
@property (strong, nonatomic) NSString *mon_open_time;
@property (strong, nonatomic) NSString *mon_close_time;
@property (strong, nonatomic) NSString *tue_open_time;
@property (strong, nonatomic) NSString *tue_close_time;
@property (strong, nonatomic) NSString *wed_open_time;
@property (strong, nonatomic) NSString *wed_close_time;
@property (strong, nonatomic) NSString *thu_open_time;
@property (strong, nonatomic) NSString *thu_close_time;
@property (strong, nonatomic) NSString *fri_open_time;
@property (strong, nonatomic) NSString *fri_close_time;
@property (strong, nonatomic) NSString *sat_open_time;
@property (strong, nonatomic) NSString *sat_close_time;
@property (strong, nonatomic) NSString *sun_open_time;
@property (strong, nonatomic) NSString *sun_close_time;

@property (assign, nonatomic) NSInteger duration;
@property (strong, nonatomic) NSString *distance;
@property (strong, nonatomic) NSString *durationText;
@property (assign,nonatomic) Boolean isFavorite;


@property (strong, nonatomic) NSString *campaign_short_desc;
@property (strong, nonatomic) NSString *validity;
@property (strong, nonatomic) NSString *age;

@end
