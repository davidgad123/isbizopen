#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>

@protocol JSONCommunicatorDelegate;

@interface JSONCommunicator : NSObject
@property (weak, nonatomic) id<JSONCommunicatorDelegate> delegate;

- (void)searchGroupsFromStore:(NSString*)storeName zipcode:(NSString*)zipcode latitude:(double)latitude longitude:(double)longitude time:(NSInteger)time day:(NSString*)day;
@end