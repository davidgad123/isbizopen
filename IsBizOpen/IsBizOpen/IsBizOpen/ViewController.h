//
//  ViewController.h
//  IsBizOpen
//
//  Created by Rolex Jain on 10/23/13.
//
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import <CoreLocation/CLLocationManager.h>
#import "JSONCommunicator.h"
#import "JSONManager.h"
#import "MBProgressHUD.h"

@interface ViewController : UIViewController <UITextFieldDelegate,CLLocationManagerDelegate, JSONManagerDelegate> {
	MBProgressHUD *HUD;
    bool wasSetZipCode;
}

@property (weak, nonatomic) IBOutlet UIButton *findButton;
@property (weak, nonatomic) IBOutlet UILabel *storeLabel;
@property (weak, nonatomic) IBOutlet UIButton *voiceButton;
@property (weak, nonatomic) IBOutlet UITextField *storeTextField;
@property (weak, nonatomic) IBOutlet UITextField *zipCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) CLLocation* currentLocation;
@property (nonatomic, retain) JSONManager* manager;
@property (nonatomic, retain) NSArray*groups;


@end
