//
//  FavoriteTableViewController.h
//  IsBizOpen
//
//  Created by Rolex Jain on 11/8/13.
//
//

#import <UIKit/UIKit.h>
#import "Group.h"
#import "MBProgressHUD.h"
#import <CoreLocation/CLLocationManager.h>

@interface FavoriteTableViewController : UITableViewController<UIGestureRecognizerDelegate>
{
    MBProgressHUD *HUD;
}

@property (strong, nonatomic) NSMutableArray *groups;
@property (strong, nonatomic) Group *selectedGroup;
@property (nonatomic, retain) CLLocation* currentLocation;

@end
