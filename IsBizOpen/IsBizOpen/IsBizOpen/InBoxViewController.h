//
//  InBoxViewController.h
//  IsBizOpen
//
//  Created by chamc on 12/13/13.
//
//

#import <UIKit/UIKit.h>


@interface InBoxViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *groups;

@end
