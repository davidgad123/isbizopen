//
//  MainChildViewController.m
//  IsBizOpen
//
//  Created by Rolex Jain on 10/24/13.
//
//

#import "MainChildViewController.h"
#import "GroupBuilder.h"
#import "Group.h"
#import <MapKit/MapKit.h>
#import "Communicator.h"
#import "MLTableAlert.h"
#import "AppDelegate.h"

@interface MainChildViewController ()

@end

@implementation MainChildViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.titleLabel setFont:[UIFont fontWithName:@"deftone stylus" size:23]];
    [self.nowLabel setFont:[UIFont fontWithName:@"century gothic" size:15]];
    [self.moreInfoLabel setFont:[UIFont fontWithName:@"century gothic" size:15]];
    [self.mapLabel setFont:[UIFont fontWithName:@"century gothic" size:15]];
    [self.callTitleLabel setFont:[UIFont fontWithName:@"century gothic" size:15]];
    [self.storeLabel setFont:[UIFont fontWithName:@"Limelight" size:20]];
    [self.addressLabel setFont:[UIFont fontWithName:@"century gothic" size:15]];
    [self.callLabel setFont:[UIFont fontWithName:@"century gothic" size:15]];
    [self.fromHoursLabel setFont:[UIFont fontWithName:@"century gothic" size:15]];
    [self.toHoursLabel setFont:[UIFont fontWithName:@"century gothic" size:15]];
    
    if (_group != nil)
    {
        NSLog(@"%@", _group.todaystiming);
        NSString* fromTime = nil;
        NSString* toTime = nil;
        
        if ([_group.is_open_24_hrs isEqualToString:@"Y"]) {
            fromTime = @"Open 24 hrs";
            toTime = nil;
        }
        else if (_group.todaystiming == nil || [_group.todaystiming isEqualToString:@""] || [_group.todaystiming isEqualToString:@"NA"])
        {
            NSString* openTime = [self getOpenTimeFromWeekday];
            NSString* closeTime = [self getCloseTimeFromWeekday];
            
            if (openTime == nil && closeTime == nil)
            {
                fromTime = @"Store timing";
                toTime = @"Unavailable";
            }
            else
            {
                fromTime = openTime;
                toTime = closeTime;
            }
        }
        else
        {
            fromTime = [_group.todaystiming substringToIndex:8];
            toTime = [_group.todaystiming substringFromIndex:12];
        }
        
        [self.fromHoursLabel setText:fromTime];
        [self.toHoursLabel setText:toTime];
        _group.poi_name = [_group.poi_name stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        [self.storeLabel setText:_group.poi_name];
        [self.addressLabel setText:_group.full_address];
        
        if ( _group.phone_number != nil && _group.phone_number.length>0)
        {
            NSString* phoneNumber = [NSString stringWithFormat:@"%@-%@-%@",[_group.phone_number substringToIndex:3],[_group.phone_number substringWithRange:NSMakeRange(3, 3)],[_group.phone_number substringFromIndex:6]];
            [self.callLabel setText:phoneNumber];
        }
        
        if (![_group.is_open_24_hrs isEqualToString:@"Y"] && [_group.openstatus isEqualToString:@"Y"])
        {
            bool willBeClose = false;
            int currentSecond = [self getSecondOfToday];
            int toSecond = [[_group.close_time stringByTrimmingCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] intValue]/100*3600;
            
            if (_group.duration != 0)
            {
                int duration = _group.duration;
                
                if ((currentSecond+duration)>toSecond)
                    willBeClose = true;
            }
            
            NSString* remainTimeText = [self remainTimeFromCurrentSecond:currentSecond toSecond:toSecond];
            [self.remainTimeLabel setText:remainTimeText];
            
            if (willBeClose)
                [self.nowImageView setImage:[UIImage imageNamed:@"open_red.png"]];
            else
                [self.nowImageView setImage:[UIImage imageNamed:@"open_green.png"]];
        }
        else if ([_group.is_open_24_hrs isEqualToString:@"Y"] && [_group.openstatus isEqualToString:@"Y"])
        {
            [self.nowImageView setImage:[UIImage imageNamed:@"open_green.png"]];
        }
        else if ([_group.openstatus isEqualToString:@"NA"])
        {
            [self.nowImageView setImage:[UIImage imageNamed:@"notime.png"]];
        }
        else
        {
            [self.nowImageView setImage:[UIImage imageNamed:@"closed"]];
        }
        
        [self.mapDistanceLabel setText:_group.distance];
        [self.mapTimeLabel setText:_group.durationText];
        
        if (_group.isFavorite)
            [_favoriteButton setImage:[UIImage imageNamed:@"favorite_1.png"] forState: UIControlStateNormal];
        else
            [_favoriteButton setImage:[UIImage imageNamed:@"favorite_0.png"] forState: UIControlStateNormal];
        
        self.moreInfos = [[NSMutableArray alloc] init];
        self.moreInfoTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 300, 300)];
        
        weekDays = [[NSArray alloc] initWithObjects:@"Monday",@"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday", @"Sunday", nil];
        
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(showMoreInfo:)];
        [self.moreInfoView addGestureRecognizer:singleFingerTap];
    }
}

- (NSString*) getOpenTimeFromWeekday
{
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* comp = [cal components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
    NSString* openTime;
    switch ([comp weekday]) {
        case 1:
            openTime = _group.sun_open_time;
            break;
        case 2:
            openTime = _group.mon_open_time;
            break;
        case 3:
            openTime = _group.tue_open_time;
            break;
        case 4:
            openTime = _group.wed_open_time;
            break;
        case 5:
            openTime = _group.thu_open_time;
            break;
        case 6:
            openTime = _group.fri_open_time;
            break;
        case 7:
            openTime = _group.sat_open_time;
            break;
    };
    
    NSString* openTimeStr = [self getHourFromData:openTime];
    
    return openTimeStr;
}

- (NSString*) getCloseTimeFromWeekday
{
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* comp = [cal components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
    NSString* time;
    switch ([comp weekday]) {
        case 1:
            time = _group.sun_close_time;
            break;
        case 2:
            time = _group.mon_close_time;
            break;
        case 3:
            time = _group.tue_close_time;
            break;
        case 4:
            time = _group.wed_close_time;
            break;
        case 5:
            time = _group.thu_close_time;
            break;
        case 6:
            time = _group.fri_close_time;
            break;
        case 7:
            time = _group.sat_close_time;
            break;
    };
    
    NSString* timeStr = [self getHourFromData:time];
    
    return timeStr;
}

- (NSString*) remainTimeFromCurrentSecond:(int)currentSecond toSecond:(int)toSecond
{
    if (toSecond>currentSecond)
    {
        int remainSecond = toSecond - currentSecond;
        int h = remainSecond/3600;
        int m = (remainSecond%3600)/60;
        return [NSString stringWithFormat:@"%dh:%dm",h,m];
    }
    else
    {
        return nil;
    }
}

- (NSInteger) getSecondOfToday
{
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components =
    [gregorian components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:today];
    
    return [components hour]*3600+[components minute]*60+[components second];
}

- (IBAction)call:(id)sender
{
    NSString *tel = [@"telprompt:" stringByAppendingString:_group.phone_number];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
}
- (IBAction)addFavorite:(id)sender {
    
    if (_group.isFavorite)
        return;
    
    [_rootViewController setFavoriteWithIndex:_index];
    _group.isFavorite = true;
    [_favoriteButton setImage:[UIImage imageNamed:@"favorite_1.png"] forState: UIControlStateNormal];
    [Communicator addFavoriteWithStoreId:_group.store_id];
    
    //update parse.com
    [Communicator refreshChannels];
}
- (void)showMoreInfo:(UITapGestureRecognizer *)recognizer {
    Group* group = [Communicator getStoreWithId:self.group.store_id];
    self.moreInfos = [self getMoreInfosWithGroup:group];
    
    if (self.moreInfos == nil) return;
    
    // create the alert
    MLTableAlert* alert = [MLTableAlert tableAlertWithTitle:@"Store Hours" cancelButtonTitle:@"OK" numberOfRows:^NSInteger (NSInteger section)
                           {
                               return self.moreInfos.count;
                           }
                                                   andCells:^UITableViewCell* (MLTableAlert *anAlert, NSIndexPath *indexPath)
                           {
                               static NSString *CellIdentifier = @"CellIdentifier";
                               UITableViewCell *cell = [anAlert.table dequeueReusableCellWithIdentifier:CellIdentifier];
                               if (cell == nil)
                               {
                                   cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                                   cell.selectionStyle = UITableViewCellSelectionStyleNone;
                               }
                               
                               cell.textLabel.text = [NSString stringWithFormat:@"%@:  %@", weekDays[indexPath.row], self.moreInfos[indexPath.row]];
                               [cell.textLabel setFont:[UIFont systemFontOfSize:15]];
                               return cell;
                           }];
    
    // Setting custom alert height
    alert.height = 420;
    
    // show the alert
    [alert show];
    
}
- (IBAction)showMap:(id)sender {
    NSString* fullAddress = [NSString stringWithString:_group.full_address];
    fullAddress = [fullAddress stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString *urlAsString = [NSString stringWithFormat:@"http://maps.google.com/maps?daddr=%@",fullAddress];
    
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    [[UIApplication sharedApplication] openURL:url];
}

- (NSMutableArray*) getMoreInfosWithGroup:(Group*)group
{
    NSMutableArray* hours = [[NSMutableArray alloc] init];
    NSString* monTimeStr = [self getStoreHourFromHourData:group.mon_open_time endTime:group.mon_close_time];
    NSString* tueTimeStr = [self getStoreHourFromHourData:group.tue_open_time endTime:group.tue_close_time];
    NSString* wedTimeStr = [self getStoreHourFromHourData:group.wed_open_time endTime:group.mon_close_time];
    NSString* thuTimeStr = [self getStoreHourFromHourData:group.thu_open_time endTime:group.mon_close_time];
    NSString* friTimeStr = [self getStoreHourFromHourData:group.fri_open_time endTime:group.mon_close_time];
    NSString* satTimeStr = [self getStoreHourFromHourData:group.sat_open_time endTime:group.mon_close_time];
    NSString* sunTimeStr = [self getStoreHourFromHourData:group.sun_open_time endTime:group.mon_close_time];
    
    if ([monTimeStr isEqualToString:@""] && [tueTimeStr isEqualToString:@""] &&
        [wedTimeStr isEqualToString:@""] && [thuTimeStr isEqualToString:@""] &&
        [friTimeStr isEqualToString:@""] && [satTimeStr isEqualToString:@""] &&
        [sunTimeStr isEqualToString:@""])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Store Hours"
                                                       message: @"Store Hours not available at this time!"
                                                      delegate: nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        [alert show];
        return nil;
    }
    [hours addObject:monTimeStr];
    [hours addObject:tueTimeStr];
    [hours addObject:wedTimeStr];
    [hours addObject:thuTimeStr];
    [hours addObject:friTimeStr];
    [hours addObject:satTimeStr];
    [hours addObject:sunTimeStr];
    
    return hours;
}

- (NSString*) getHourFromData:(NSString*)timeData
{
    if ([timeData isEqualToString:@""] || timeData == nil) {
        return nil;
    }
    int hour = [[timeData stringByTrimmingCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] intValue]/100;
    
    NSString* hourStr;
    
    if (hour>12)
    {
        hour -= 12;
        hourStr = [NSString stringWithFormat:@"%d:%@ PM", hour, [timeData substringFromIndex:2]];
    }
    else
    {
        hourStr = [NSString stringWithFormat:@"%d:%@ AM", hour, [timeData substringFromIndex:2]];
    }
    
    return hourStr;
}
- (NSString*) getStoreHourFromHourData:(NSString*)openTime endTime:(NSString*)endTime
{
    if ([openTime isEqualToString:@""] || [endTime isEqualToString:@""]) {
        return @"";
    }
    
    NSString* openHourStr = [self getHourFromData:openTime];
    NSString* endHourStr = [self getHourFromData:endTime];
    
    NSString* hour = [NSString stringWithFormat:@"%@ - %@", openHourStr, endHourStr];
    
    return hour;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
