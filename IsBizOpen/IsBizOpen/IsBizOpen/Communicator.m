//
//  Communicator.m
//  IsBizOpen
//
//  Created by Rolex Jain on 11/10/13.
//
//

#import "Communicator.h"
#import "GroupBuilder.h"
#import <Parse/Parse.h>

@implementation Communicator

+(NSArray*) getFavorites
{
    NSString *deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];;
    
        NSString *req = [NSString stringWithFormat: @"http://isbizopen.com/api/manageStoreFav.php?type=show&storeid=&email=%@",deviceId];
   // NSString *req = [NSString stringWithFormat: @"http://isbizopen.com/api/manageStoreFav.php?type=show&storeid=&email=testuser@gmail.com"];
    NSLog(@"show favorite url:%@", req);
    NSString *response = [NSString stringWithContentsOfURL: [NSURL URLWithString: req] encoding: NSUTF8StringEncoding error: NULL];
    
    if (response == nil) return nil;
    
    NSData* data = [response dataUsingEncoding:NSUTF8StringEncoding];
    NSArray* groups = [GroupBuilder groupsFromJSON:data error:nil];    
    return groups;
}

+(NSArray*) getInboxes
{
    NSString *deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];;
    
    NSString *req = [NSString stringWithFormat: @"http://isbizopen.com/api/getUserInbox.php?email=%@",deviceId];
    //NSString *req = [NSString stringWithFormat: @"http://isbizopen.com/api/getUserInbox.php?email=1D0E1F94-D8D0-40F8-9432-47D37490E6F1"];
    NSLog(@"show inbox url:%@", req);
    NSString *response = [NSString stringWithContentsOfURL: [NSURL URLWithString: req] encoding: NSUTF8StringEncoding error: NULL];
    
    if (response == nil) return nil;
    
    NSData* data = [response dataUsingEncoding:NSUTF8StringEncoding];
    NSArray* groups = [GroupBuilder groupsFromJSON:data error:nil];
    return groups;
}

+ (void) addFavoriteWithStoreId:(NSString*)storeId
{
    NSString *deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *req = [NSString stringWithFormat: @"http://isbizopen.com/api/manageStoreFav.php?type=add&storeid=%@&email=%@",storeId,deviceId];
    
    NSLog(@"add favorite url:%@", req);
    [NSString stringWithContentsOfURL: [NSURL URLWithString: req] encoding: NSUTF8StringEncoding error: NULL];
}

+ (void) removeFavoriteWithStoreId:(NSString*)storeId
{
    NSString *deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *req = [NSString stringWithFormat: @"http://isbizopen.com/api/manageStoreFav.php?type=remove&storeid=%@&email=%@",storeId,deviceId];
    
    NSLog(@"remove favorite url:%@", req);
    [NSString stringWithContentsOfURL: [NSURL URLWithString: req] encoding: NSUTF8StringEncoding error: NULL];
}

+(Group*) getStoreWithId:(NSString*)storeId
{
    NSString *req = [NSString stringWithFormat: @"http://isbizopen.com/api/getStoreDetails.php?storeid=%@",storeId];
    NSLog(@"get store url:%@", req);
    NSString *response = [NSString stringWithContentsOfURL: [NSURL URLWithString: req] encoding: NSUTF8StringEncoding error: NULL];
    
    if (response == nil) return nil;
    
    NSData* data = [response dataUsingEncoding:NSUTF8StringEncoding];
    NSArray* groups = [GroupBuilder groupsFromJSON:data error:nil];
    
    if (groups == nil || groups.count<=0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert"
                                                       message: @"You have not favorited any stores yet."
                                                      delegate: nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        [alert show];
        return nil;
    }
    
    return groups[0];
}

+ (Group*)setupDurationAndDistanceWithGroup:(Group*)group latitude:(float)latitude longitude:(float)longitude
{
    NSString* fullAddress = [NSString stringWithString:group.full_address];
    fullAddress = [fullAddress stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString* startAddress = [NSString stringWithFormat:@"%f,%f", latitude, longitude];
    
    NSString *req = [NSString stringWithFormat: @"http://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&sensor=false", startAddress, fullAddress];
    //NSString *req = [NSString stringWithFormat: @"http://maps.googleapis.com/maps/api/directions/json?origin=43.7001100,-79.4163000&destination=%@&sensor=false", fullAddress];
    NSLog(@"url:%@", req);
    NSString *googleResponse = [NSString stringWithContentsOfURL: [NSURL URLWithString: req] encoding: NSUTF8StringEncoding error: NULL];
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[googleResponse dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSString *status = [result valueForKey: @"status"];
    
    if ([status isEqualToString:@"OK"])
    {
        NSArray* routes = [result valueForKey: @"routes"];
        NSDictionary *routesDict = [routes objectAtIndex:0];
        
        NSArray *legs = [routesDict objectForKey:@"legs"];
        NSDictionary *legsDict = [legs objectAtIndex:0];
        NSDictionary *durationDict = [legsDict valueForKey: @"duration"];
        NSNumber *durationValue = [durationDict valueForKey: @"value"];
        NSString *durationText = [durationDict valueForKey: @"text"];
        NSDictionary *distanceDict = [legsDict valueForKey: @"distance"];
        NSString *distanceText = [distanceDict valueForKey: @"text"];
        
        group.duration = [durationValue intValue];
        group.durationText = durationText;
        group.distance = distanceText;
    }
    else
        group.duration = 0;
    
    return group;
}

+ (NSArray*)setupDurationAndDistanceWithGroups:(NSArray*)groups latitude:(float)latitude longitude:(float)longitude
{
    for (Group *group in groups) {
        NSString* fullAddress = [NSString stringWithString:group.full_address];
        fullAddress = [fullAddress stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        NSString* startAddress = [NSString stringWithFormat:@"%f,%f", latitude, longitude];
        
        NSString *req = [NSString stringWithFormat: @"http://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&sensor=false", startAddress, fullAddress];
        //       NSString *req = [NSString stringWithFormat: @"http://maps.googleapis.com/maps/api/directions/json?origin=43.7001100,-79.4163000&destination=%@&sensor=false", fullAddress];
        NSLog(@"url:%@", req);
        NSString *googleResponse = [NSString stringWithContentsOfURL: [NSURL URLWithString: req] encoding: NSUTF8StringEncoding error: NULL];
        
        if (googleResponse == nil) continue;
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:[googleResponse dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        NSString *status = [result valueForKey: @"status"];
        
        if ([status isEqualToString:@"OK"])
        {
            NSArray* routes = [result valueForKey: @"routes"];
            NSDictionary *routesDict = [routes objectAtIndex:0];
            
            NSArray *legs = [routesDict objectForKey:@"legs"];
            NSDictionary *legsDict = [legs objectAtIndex:0];
            NSDictionary *durationDict = [legsDict valueForKey: @"duration"];
            NSNumber *durationValue = [durationDict valueForKey: @"value"];
            NSString *durationText = [durationDict valueForKey: @"text"];
            NSDictionary *distanceDict = [legsDict valueForKey: @"distance"];
            NSString *distanceText = [distanceDict valueForKey: @"text"];
            
            group.duration = [durationValue intValue];
            group.durationText = durationText;
            group.distance = distanceText;
        }
        else
            group.duration = 0;
    }
    
    return groups;
}

+(void) refreshChannels
{
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    NSMutableArray* channels = [[NSMutableArray alloc] init];
    NSArray* favoriteStores = [Communicator getFavorites];
    
    for (int i=0; i<favoriteStores.count; i++) {
        Group* group = favoriteStores[i];
        NSString* channel = [NSString stringWithFormat:@"c%@",group.store_id];
        [channels addObject:channel];
    }
    
    [currentInstallation setChannels:channels];
    [currentInstallation saveInBackground];
}

@end
