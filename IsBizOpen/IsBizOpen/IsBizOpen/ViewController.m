//
//  ViewController.m
//  IsBizOpen
//
//  Created by Rolex Jain on 10/23/13.
//
//

#import "ViewController.h"
#import "RootViewController.h"
#import <CoreLocation/CLGeocoder.h>
#import <CoreLocation/CLPlacemark.h>
#import <CoreLocation/CLLocationManager.h>
#import <MapKit/MKReverseGeocoder.h>
#import "FavoriteTableViewController.h"
#import "GroupBuilder.h"
#import <AddressBook/AddressBook.h>
#import <UIKit/UIDevice.h>
#import "Communicator.h"
#import "InBoxViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    [self.storeLabel setFont:[UIFont fontWithName:@"Reenie Beanie" size:25]];
    self.storeTextField.delegate = self;
    self.zipCodeTextField.delegate = self;
    [self findCurrentLocation];
    
    // loading progress
	HUD = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:HUD];
    [self.navigationController setNavigationBarHidden:YES];
    
    wasSetZipCode = false;
    
    // hide voice button
    [self.voiceButton setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!self.navigationController.navigationBarHidden)
        [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [HUD hide:false];
}
- (IBAction)showInboxes:(id)sender {
    
    _groups = [Communicator getInboxes];
    
    if (_groups == nil || _groups.count<=0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert"
                                                       message: @"Start favoriting stores to receive Deals and Notifications"
                                                      delegate: nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        [alert show];
        return;
    }
    
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self performSegueWithIdentifier:@"inboxSegue" sender:self];
    });
}

- (IBAction)showFavoriteStores:(id)sender {
    _groups = [Communicator getFavorites];
    
    if (_groups == nil || _groups.count<=0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert"
                                                       message: @"You have not favorited any stores yet."
                                                      delegate: nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        [alert show];
        return;
    }
    
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self performSegueWithIdentifier:@"favoriteSegue" sender:self];
    });
}

- (IBAction)findIt:(id)sender
{
    if ([self.storeTextField.text length] <= 0 || [self.zipCodeTextField.text length] <= 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Input"
                                                       message: @"Please input store and zipcode."
                                                      delegate: nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        [alert show];
        return;
    }
  
    [self.view endEditing:YES];
    //show loading progress
	HUD.labelText = @"Fetching store details";
    [HUD show:true];
    
    // json data
    _manager = [[JSONManager alloc] init];
    _manager.communicator = [[JSONCommunicator alloc] init];
    _manager.communicator.delegate = _manager;
    _manager.delegate = self;
    
    [_manager fetchGroupsFromStore:[self.storeTextField text] zipcode:[self.zipCodeTextField text] latitude:self.currentLocation.coordinate.latitude longitude:self.currentLocation.coordinate.longitude time:[self getHourOfToday]*100 day:[self getWeekDay]];
}

- (NSInteger) getHourOfToday
{
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc]
                              initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components =
    [gregorian components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:today];
    
    return [components hour];
}

- (NSString*) getWeekDay
{
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* comp = [cal components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
    NSString* weekday;
    switch ([comp weekday]) {
        case 1:
            weekday = @"sunday";
            break;
        case 2:
            weekday = @"monday";
            break;
        case 3:
            weekday = @"tuesday";
            break;
        case 4:
            weekday = @"wednesday";
            break;
        case 5:
            weekday = @"thursday";
            break;
        case 6:
            weekday = @"friday";
            break;
        case 7:
            weekday = @"saturday";
            break;
    };
    return weekday;
}

// find current location
-(void)findCurrentLocation
{
    _locationManager = [[CLLocationManager alloc] init];
    if ([CLLocationManager locationServicesEnabled])
    {
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        [_locationManager startUpdatingLocation];
    }
}

// get zipCode from location and set it in zipcode textfiled.
- (void)reverseGeocodeLocation:(CLLocation *)location
{
    CLGeocoder* reverseGeocoder = [[CLGeocoder alloc] init];
    if (reverseGeocoder) {
        [reverseGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            CLPlacemark* placemark = [placemarks firstObject];
            if (placemark) {
                //Using blocks, get zip code
                NSString *zipCode = [placemark.addressDictionary objectForKey:(NSString*) kABPersonAddressZIPKey];
                
                if (zipCode != nil && !wasSetZipCode)
                {
                    [self.zipCodeTextField setText:zipCode];
                    wasSetZipCode = true;
                }
            }
        }];
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString: @"searchSegue"]) {
        RootViewController *dest = (RootViewController *)[segue destinationViewController];
        //the sender is what you pass into the previous method
        dest.groups=self.groups;
    } else if ([[segue identifier] isEqualToString: @"favoriteSegue"]) {
        FavoriteTableViewController *dest = (FavoriteTableViewController *)[segue destinationViewController];
        //the sender is what you pass into the previous method
        dest.groups = [self.groups mutableCopy];
        dest.currentLocation = self.currentLocation;
    } else if ([[segue identifier] isEqualToString: @"inboxSegue"]) {
        InBoxViewController *dest = (InBoxViewController *)[segue destinationViewController];
        //the sender is what you pass into the previous method
        dest.groups = [self.groups mutableCopy];
    }
}

#pragma mark - UITextFieldDelegate
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    self.currentLocation = newLocation;
    [self reverseGeocodeLocation:newLocation];
}

#pragma mark - MeetupManagerDelegate
- (void)didReceiveGroups:(NSArray *)groups
{
    self.groups = [Communicator setupDurationAndDistanceWithGroups:groups latitude:self.currentLocation.coordinate.latitude longitude:self.currentLocation.coordinate.longitude];
    
    //[HUD hide:false];
    
    if (groups == nil || groups.count<=0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Search Error"
                                                       message: @"No result found. Please try again."
                                                      delegate: nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil,nil];
        [alert show];
        return;
    }
    
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self performSegueWithIdentifier:@"searchSegue" sender:self];
    });
}

- (void)fetchingGroupsFailedWithError:(NSError *)error
{
    // hide loading progress
    [HUD hide:false];
    
    NSLog(@"Error %@; %@", error, [error localizedDescription]);
}

@end
