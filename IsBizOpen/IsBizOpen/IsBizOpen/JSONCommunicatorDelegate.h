//
//  JSONCommunicatorDelegate.h
//

#import <Foundation/Foundation.h>

@protocol JSONCommunicatorDelegate
- (void)receivedGroupsJSON:(NSData *)objectNotation;
- (void)fetchingGroupsFailedWithError:(NSError *)error;
@end
