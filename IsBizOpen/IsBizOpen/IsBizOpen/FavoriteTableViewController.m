//
//  FavoriteTableViewController.m
//  IsBizOpen
//
//  Created by Rolex Jain on 11/8/13.
//
//

#import "FavoriteTableViewController.h"
#import "StoreViewCell.h"
#import "Group.h"
#import "RootViewController.h"
#import "SIAlertView.h"
#import "Communicator.h"
#import "AppDelegate.h"

@interface FavoriteTableViewController ()

@end

@implementation FavoriteTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.navigationController setNavigationBarHidden:NO];
    [self setTitle:@"Favorite Stores"];
    
	HUD = [[MBProgressHUD alloc] initWithView:self.view];
	HUD.labelText = @"Fetching store details";
	[self.view addSubview:HUD];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _groups.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"StoreCell";
    
    StoreViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[StoreViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                     reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    Group* group = _groups[indexPath.row];
    cell.title.text = group.poi_name;
    cell.subTitle.text = group.full_address;
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(LongPressGestureRecognizer:)];
    [cell addGestureRecognizer:lpgr];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [HUD show:true];
    
    _selectedGroup = _groups[indexPath.row];
    _selectedGroup = [Communicator getStoreWithId:_selectedGroup.store_id];
    _selectedGroup = [Communicator setupDurationAndDistanceWithGroup:_selectedGroup latitude:self.currentLocation.coordinate.latitude longitude:self.currentLocation.coordinate.longitude];
    _selectedGroup.isFavorite = true;
    
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [HUD hide:false];
        [self performSegueWithIdentifier:@"storeSegue" sender:self];
    });
    
	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)LongPressGestureRecognizer:(UIGestureRecognizer *)gr
{
    if (gr.state == UIGestureRecognizerStateBegan)
    {
        CGPoint p = [gr locationInView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
        
        if (indexPath == nil)
            return;
        
        Group* group = _groups[indexPath.row];
        NSString* storeId = group.store_id;
        
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:nil];
        [alertView addButtonWithTitle:@"Remove Favorite"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                                  [Communicator removeFavoriteWithStoreId:storeId];
                                  [_groups removeObjectAtIndex: indexPath.row];
                                  [self.tableView reloadData];
                                  
                                  //refresh parse.com
                                  [Communicator refreshChannels];
                              }];
        [alertView addButtonWithTitle:@"Copy To Clipboard"
                                 type:SIAlertViewButtonTypeDefault
                              handler:^(SIAlertView *alertView) {
                                  UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                                  pasteboard.string = [NSString stringWithFormat:@"%@\n\n%@",group.poi_name,group.full_address];
                              }];
        [alertView addButtonWithTitle:@"Cancel"
                                 type:SIAlertViewButtonTypeCancel
                              handler:^(SIAlertView *alertView) {
                              }];
        [alertView show];
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString: @"storeSegue"]) {
        RootViewController *dest = (RootViewController *)[segue destinationViewController];
        //the sender is what you pass into the previous method
        dest.groups=[[NSArray alloc] initWithObjects:_selectedGroup, nil];
    }
}

@end
